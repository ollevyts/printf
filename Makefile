# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/09/13 14:16:03 by ollevyts          #+#    #+#              #
#    Updated: 2018/09/13 14:16:05 by ollevyts         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a
FLAGS = -Wall -Wextra -Werror

SRC_DIR = src/
LIB_DIR = libft/

SRC_FILES = ft_printf_attr.c ft_printf_bin.c ft_printf_hex.c ft_printf_nbr.c \
			ft_printf_oct.c ft_printf_signed.c ft_printf_spec.c \
			ft_printf_text.c ft_printf_unsigned.c ft_printf.c ft_pr_other.c \
			ft_pr_other1.c
LIBFT_FILES = ft_atoi.c ft_bzero.c ft_isdigit.c ft_memalloc.c ft_putchar.c \
			  ft_putnchar.c ft_putstr.c ft_putunbr.c ft_putwchar.c \
			  ft_putwstr.c ft_strdup.c ft_strlen.c ft_strncpy.c ft_strrev.c \
			  ft_strsub.c ft_strtolower.c ft_wclen.c ft_wcscpy.c ft_wcsdup.c \
			  ft_wcslen.c ft_wcsncpy.c ft_wcsnew.c ft_strcpy.c

SRC = $(addprefix $(SRC_DIR), $(SRC_FILES))
LIBFT = $(addprefix $(LIB_DIR), $(LIBFT_FILES))

OBJ = $(SRC_FILES:.c=.o) $(LIBFT_FILES:.c=.o)

INC = -Iincludes/
FLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME):
	@echo "Compiling... "
	@gcc -c $(FLAGS) $(SRC) $(LIBFT) $(INC)
	@ar rc $(NAME) $(OBJ)
	@ranlib $(NAME)
	@echo "$(NAME) generated!".

clean:
	@rm -f $(OBJ)
	@echo "OBJ-files generated!".

fclean: clean
	@rm -f $(NAME)
	@echo "all cleaned!".

re: fclean all

.PHONY:clean fclean all re
