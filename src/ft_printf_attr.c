/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_attr.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 14:40:14 by ollevyts          #+#    #+#             */
/*   Updated: 2018/09/13 14:40:15 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

static int	get_flags(char flg, t_attr *attr)
{
	if (flg == '#')
		attr->flags |= HASH;
	else if (flg == '0')
		attr->flags |= ZERO;
	else if (flg == '-')
		attr->flags |= MINUS;
	else if (flg == '+')
		attr->flags |= PLUS;
	else if (flg == ' ')
		attr->flags |= SPACE;
	return (flg == '#' || flg == '0' || flg == '-' || flg == ' ' || flg == '+');
}

static void	if_func(t_attr *attr, char *nbr, int precision)
{
	if (precision != 1)
		attr->precision = ft_atoi(nbr);
	else
		attr->precision = 0;
}

static int	get_precision(va_list ap, char *format, t_attr *attr)
{
	int		precision;
	char	*nbr;

	precision = 0;
	if (format[0] == '.')
	{
		precision = 1;
		if (format[precision] == '*')
		{
			attr->precision = va_arg(ap, int);
			precision++;
		}
		else
		{
			while (ft_isdigit(format[precision]))
				precision++;
			if (!(nbr = ft_strsub(format, 1, precision)))
				return (0);
			if_func(attr, nbr, precision);
			free(nbr);
		}
	}
	return (precision);
}

static void	prop_attr(t_attr *attr, int i, char **format)
{
	if (i == 0)
		attr->flags |= WIDTH;
	if (i == 1)
		if (attr->precision >= 0)
			attr->flags |= PRECISION;
	(*format) += attr->format_width;
}

int			get_attributes(char **format, va_list arg, t_attr *attr)
{
	attr->init = *format;
	while (get_flags(**format, attr))
		(*format)++;
	while ((attr->format_width = get_width(arg, *format, attr)))
		prop_attr(attr, 0, format);
	while ((attr->format_width = get_precision(arg, *format, attr)))
		prop_attr(attr, 1, format);
	while ((attr->format_width = get_length(*format, attr)))
		prop_attr(attr, 2, format);
	return (*format > attr->init);
}
