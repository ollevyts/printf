/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pr_other1.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 16:00:26 by ollevyts          #+#    #+#             */
/*   Updated: 2018/09/13 16:00:29 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

static void	prop_zr(t_attr *attr, int *len, int sign, int i)
{
	if (i == 0)
		attr->nb_zr += attr->precision - *len + sign;
	if (i == 1)
		attr->nb_zr += attr->width - *len;
	*len += attr->nb_zr;
}

int			get_nbr_zeroes(t_attr *attr, int *len, int sign)
{
	attr->nb_zr = 0;
	if (attr->flags & PRECISION && attr->precision > *len - sign)
		prop_zr(attr, len, sign, 0);
	else if (attr->flags & WIDTH && attr->flags & ZERO &&
		!(attr->flags & MINUS) && !(attr->flags & PRECISION) &&
		attr->width > *len)
		prop_zr(attr, len, sign, 1);
	return (attr->nb_zr);
}

int			get_nbr_spaces(int attr, int minw, int *len)
{
	int		nbr_spaces;

	nbr_spaces = 0;
	if (attr & WIDTH && (!(attr & ZERO) ||
		attr & MINUS || attr & PRECISION) && minw > *len)
	{
		nbr_spaces += minw - *len;
		*len += nbr_spaces;
	}
	return (nbr_spaces);
}

static int	if_no(uintmax_t nbr)
{
	int j;

	if (!nbr)
		j = 1;
	else
		j = 0;
	return (j);
}

char		*get_nbr_unsigned(uintmax_t nbr, int *nbr_digits, int base)
{
	int			len;
	char		*result;
	uintmax_t	reminder;

	len = 0;
	reminder = nbr;
	*nbr_digits = if_no(nbr);
	while (reminder)
	{
		reminder /= base;
		*nbr_digits += 1;
	}
	if (!(result = (char *)malloc(sizeof(char) * (*nbr_digits))))
		return (NULL);
	*result = '0';
	len = if_no(nbr);
	while (nbr)
	{
		reminder = nbr % base;
		nbr /= base;
		result[len++] = reminder + '0';
	}
	result[len] = '\0';
	result = ft_strrev(result);
	return (result);
}
