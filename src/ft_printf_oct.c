/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_oct.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 14:40:45 by ollevyts          #+#    #+#             */
/*   Updated: 2018/09/13 14:40:46 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

static char	*convert_octal(uintmax_t nbr, int *nbdig)
{
	char	*i;

	i = get_nbr_unsigned(nbr, nbdig, 8);
	return (i);
}

static void	pr_oct(va_list ap, t_attr *attr)
{
	if (attr->flags & L || attr->flags & UPP_O_BIT)
		attr->oct_len = format_unsigned(
			va_arg(ap, unsigned long), attr, &convert_octal);
	else if (attr->flags & H)
		attr->oct_len = format_unsigned(
			(unsigned short)va_arg(ap, unsigned int), attr, &convert_octal);
	else if (attr->flags & HH)
		attr->oct_len = format_unsigned(
			(unsigned char)va_arg(ap, unsigned int), attr, &convert_octal);
	else
		attr->oct_len = format_unsigned(
			va_arg(ap, unsigned int), attr, &convert_octal);
}

int			print_octal(va_list ap, t_attr *attr)
{
	if (attr->flags & J)
		attr->oct_len = format_unsigned(
			va_arg(ap, uintmax_t), attr, &convert_octal);
	else if (attr->flags & Z)
		attr->oct_len = format_unsigned(
			va_arg(ap, size_t), attr, &convert_octal);
	else if (attr->flags & LL)
		attr->oct_len = format_unsigned(
			va_arg(ap, unsigned long long), attr, &convert_octal);
	pr_oct(ap, attr);
	return (attr->oct_len);
}
