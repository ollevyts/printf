/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_signed.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 14:40:59 by ollevyts          #+#    #+#             */
/*   Updated: 2018/09/13 14:41:00 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

static int	nbr_digits(intmax_t nbr)
{
	int		len;

	if (!nbr)
		return (1);
	if (nbr < 0)
		len = 1;
	else
		len = 0;
	while (nbr)
	{
		nbr /= 10;
		len++;
	}
	return (len);
}

static void	put_sign(intmax_t nbr, t_attr *attr)
{
	attr->p_sign = 0;
	if (nbr < 0)
		attr->p_sign = '-';
	else if (nbr >= 0)
	{
		if (attr->flags & PLUS)
			attr->p_sign = '+';
		else if (attr->flags & SPACE)
			attr->p_sign = ' ';
	}
	if (attr->p_sign)
		ft_putchar(attr->p_sign);
}

static void	check(intmax_t nbr, t_attr *attr)
{
	if (nbr >= 0 && (attr->flags & PLUS || attr->flags & SPACE))
		attr->s_sign = 1;
	else
		attr->s_sign = 0;
	if (!(attr->flags & PRECISION) || attr->precision || nbr)
		attr->s_len = nbr_digits(nbr);
}

static void	check1(intmax_t nbr, t_attr *attr)
{
	if (!(attr->flags & MINUS))
		ft_putnchar(' ', attr->s_nbr_spaces);
	put_sign(nbr, attr);
	ft_putnchar('0', attr->s_nbr_zeros);
	if (!(attr->flags & PRECISION && !attr->precision && !nbr))
	{
		if (nbr < 0)
			ft_putunbr(-nbr);
		else
			ft_putunbr(nbr);
	}
	if (attr->flags & MINUS)
		ft_putnchar(' ', attr->s_nbr_spaces);
}

int			format_signed(intmax_t nbr, t_attr *attr)
{
	attr->s_len = 0;
	check(nbr, attr);
	attr->s_len += attr->s_sign;
	attr->s_nbr_zeros = get_nbr_zeroes(
		attr, &attr->s_len, (nbr < 0 || attr->s_sign));
	attr->s_nbr_spaces = get_nbr_spaces(
		attr->flags, attr->width, &attr->s_len);
	check1(nbr, attr);
	return (attr->s_len);
}
