/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pr_other.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 15:52:53 by ollevyts          #+#    #+#             */
/*   Updated: 2018/09/13 15:52:54 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int			print_spaces(int width, int len, int attr)
{
	while ((width - len) > 0)
	{
		if ((attr & ZERO) && !(attr & MINUS))
			ft_putchar('0');
		else
			ft_putchar(' ');
		len++;
	}
	return (len);
}

static void	prop_len(t_attr *attr, char *format, int i)
{
	if (i == 0)
	{
		if (format[attr->g_length + 1] == 'h')
			attr->flags |= HH;
		else
			attr->flags |= H;
	}
	if (i == 1)
	{
		if (format[attr->g_length + 1] == 'l')
			attr->flags |= LL;
		else
			attr->flags |= L;
	}
}

int			get_length(char *format, t_attr *attr)
{
	attr->g_length = 0;
	while (format[attr->g_length] == 'h' || format[attr->g_length] == 'l' \
			|| format[attr->g_length] == 'j' || format[attr->g_length] == 'z')
	{
		if (format[attr->g_length] == 'h')
			prop_len(attr, format, 0);
		if (format[attr->g_length] == 'l')
			prop_len(attr, format, 1);
		if (attr->flags == HH || attr->flags == LL)
			attr->g_length++;
		if (format[attr->g_length] == 'j')
			attr->flags |= J;
		if (format[attr->g_length] == 'z')
			attr->flags |= Z;
		attr->g_length++;
	}
	return (attr->g_length);
}

static void	if_else(va_list arg, char *format, t_attr *attr, int i)
{
	if (i == 1)
	{
		attr->width = va_arg(arg, int);
		if (attr->width < 0)
		{
			attr->width *= -1;
			attr->flags |= MINUS;
		}
		attr->g_width = 1;
	}
	else
	{
		while (ft_isdigit(format[attr->g_width]))
			attr->g_width++;
		if (!(attr->g_width) ||
			!(attr->gw_nbr = ft_strsub(format, 0, attr->g_width)))
			return ;
		attr->width = ft_atoi(attr->gw_nbr);
		free(attr->gw_nbr);
	}
}

int			get_width(va_list arg, char *format, t_attr *attr)
{
	attr->g_width = 0;
	if (format[attr->g_width] == '*')
		if_else(arg, format, attr, 1);
	else
		if_else(arg, format, attr, 2);
	return (attr->g_width);
}
