/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_bin.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 14:40:21 by ollevyts          #+#    #+#             */
/*   Updated: 2018/09/13 14:40:22 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

static char	*conv_bin(uintmax_t nbr, int *nbr_digits)
{
	char	*i;

	i = get_nbr_unsigned(nbr, nbr_digits, 2);
	return (i);
}

static void	pr_bin(va_list ap, t_attr *attr)
{
	if (attr->flags & L || attr->flags & UPP_O_BIT)
		attr->bin_len = format_unsigned(va_arg(ap, unsigned long), attr,
			&conv_bin);
	else if (attr->flags & H)
		attr->bin_len = format_unsigned(
			(unsigned short)va_arg(ap, unsigned int), attr, &conv_bin);
	else if (attr->flags & HH)
		attr->bin_len = format_unsigned(
			(unsigned char)va_arg(ap, unsigned int),
			attr, &conv_bin);
	else
		attr->bin_len = format_unsigned(
			va_arg(ap, unsigned int), attr, &conv_bin);
}

int			print_binary(va_list ap, t_attr *attr)
{
	if (attr->flags & J)
		attr->bin_len = format_unsigned(va_arg(ap, uintmax_t), attr, &conv_bin);
	else if (attr->flags & Z)
		attr->bin_len = format_unsigned(va_arg(ap, size_t), attr, &conv_bin);
	else if (attr->flags & LL)
		attr->bin_len = format_unsigned(va_arg(ap, unsigned long long), attr, \
								&conv_bin);
	pr_bin(ap, attr);
	return (attr->bin_len);
}
