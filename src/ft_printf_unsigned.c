/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_unsigned.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 14:41:24 by ollevyts          #+#    #+#             */
/*   Updated: 2018/09/13 14:41:25 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

static int	not_decimal(uintmax_t nbr, int attr, int prec)
{
	if ((attr & LOW_O_BIT || attr & UPP_O_BIT) && attr & HASH &&
		(nbr || (!nbr && attr & PRECISION && !prec)))
		return (1);
	else if (((attr & LOW_X_BIT || attr & UPP_X_BIT) &&
		attr & HASH && nbr) || attr & LOW_P_BIT)
		return (2);
	return (0);
}

static void	ft_first(uintmax_t nbr, t_attr *attr)
{
	if (attr->flags & LOW_X_BIT || attr->flags & LOW_P_BIT)
		ft_str_to_lowcase(attr->uns_nbr_converted);
	if (attr->flags & PRECISION && !(attr->precision) && !nbr)
		attr->uns_len = 0;
	attr->uns_len += attr->uns_sign;
	if (attr->uns_sign == 2)
		attr->uns_i = 2;
	else
		attr->uns_i = 0;
}

static void	ft_last(uintmax_t nbr, t_attr *attr)
{
	int		nbr_zeros;
	int		nbr_spaces;

	nbr_zeros = get_nbr_zeroes(attr, &attr->uns_len, attr->uns_i);
	nbr_spaces = get_nbr_spaces(attr->flags, attr->width, &attr->uns_len);
	if (!(attr->flags & MINUS))
		ft_putnchar(' ', nbr_spaces);
	if (not_decimal(nbr, attr->flags, attr->precision))
		ft_putchar('0');
	if (not_decimal(nbr, attr->flags, attr->precision) > 1)
	{
		if (attr->flags & UPP_X_BIT)
			ft_putchar('X');
		else
			ft_putchar('x');
	}
	ft_putnchar('0', nbr_zeros);
	if (!(attr->flags & PRECISION && !attr->precision && !nbr))
		ft_putstr(attr->uns_nbr_converted);
	free(attr->uns_nbr_converted);
	if (attr->flags & MINUS)
		ft_putnchar(' ', nbr_spaces);
}

int			format_unsigned(uintmax_t nbr, t_attr *attr,
	char *(*convert)(uintmax_t, int *))
{
	attr->uns_nbr_converted = convert(nbr, &attr->uns_len);
	attr->uns_sign = not_decimal(nbr, attr->flags, attr->precision);
	ft_first(nbr, attr);
	ft_last(nbr, attr);
	return (attr->uns_len);
}
