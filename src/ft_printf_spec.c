/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_spec.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 14:41:06 by ollevyts          #+#    #+#             */
/*   Updated: 2018/09/13 14:41:09 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

static int	specifier_position(char c, char *spec)
{
	int		pos;

	pos = 0;
	while (spec[pos])
	{
		if (c == spec[pos])
			return (pos);
		pos++;
	}
	return (-1);
}

static void	get_specifier(t_attr *attr)
{
	attr->s_corres[LOWER_O] = LOW_O_BIT;
	attr->s_corres[UPPER_O] = UPP_O_BIT;
	attr->s_corres[LOWER_U] = LOW_U_BIT;
	attr->s_corres[UPPER_U] = UPP_U_BIT;
	attr->s_corres[LOWER_X] = LOW_X_BIT;
	attr->s_corres[UPPER_X] = UPP_X_BIT;
	attr->s_corres[LOWER_C] = LOW_C_BIT;
	attr->s_corres[UPPER_C] = UPP_C_BIT;
	attr->s_corres[LOWER_B] = LOW_B_BIT;
}

static int	get_specifier1(char c, t_attr *attr, char *spec)
{
	attr->s_corres = ft_memalloc(sizeof(int) * SPECI_SIZE);
	attr->s_corres[LOWER_S] = LOW_S_BIT;
	attr->s_corres[UPPER_S] = UPP_S_BIT;
	attr->s_corres[LOWER_P] = LOW_P_BIT;
	attr->s_corres[LOWER_D] = LOW_D_BIT;
	attr->s_corres[UPPER_D] = UPP_D_BIT;
	attr->s_corres[LOWER_I] = LOW_I_BIT;
	get_specifier(attr);
	if (((attr->s_specifier = specifier_position(c, spec)) != -1) && c)
		attr->flags |= attr->s_corres[attr->s_specifier];
	free(attr->s_corres);
	return (attr->s_specifier);
}

static void	while_parse(t_attr *attr, va_list arg, char **format)
{
	while (**format)
	{
		if ((specifier_position(**format, attr->p_conversion) != -1)
				&& (attr->p_specifier = get_specifier1(
					**format, attr, attr->p_conversion)))
			break ;
		if (!(attr->p_is_valid = get_attributes(format, arg, attr)))
			break ;
	}
}

int			parse_specifier(va_list arg, char **format, t_attr *attr)
{
	attr->flags = 0;
	if (!(attr->p_conversion = (char *)malloc(sizeof(char) * SPECI_SIZE)))
		return (0);
	attr->p_conversion = ft_strncpy(attr->p_conversion, SPECIFIERS, SPECI_SIZE);
	attr->p_specifier = -1;
	while_parse(attr, arg, format);
	free(attr->p_conversion);
	return (attr->p_specifier);
}
