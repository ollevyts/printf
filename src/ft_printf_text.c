/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_text.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 14:41:16 by ollevyts          #+#    #+#             */
/*   Updated: 2018/09/13 14:41:18 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int			print_wide_string(va_list ap, t_attr *attr)
{
	if (!(attr->ws_str = va_arg(ap, wchar_t *)))
		attr->ws_str = ft_wcsdup(L"(null)");
	if (attr->flags & PRECISION)
	{
		attr->ws_cpy = ft_wcsnew(attr->precision);
		if (attr->ws_str)
			attr->ws_cpy = ft_wcsncpy(
				attr->ws_cpy, attr->ws_str, attr->precision);
		attr->ws_str = attr->ws_cpy;
	}
	attr->ws_len = ft_wcslen(attr->ws_str);
	if (!(attr->flags & MINUS))
		attr->ws_len = print_spaces(attr->width, attr->ws_len, attr->flags);
	ft_putwstr(attr->ws_str);
	if (attr->flags & MINUS)
		attr->ws_len = print_spaces(attr->width, attr->ws_len, attr->flags);
	return (attr->ws_len);
}

static void	prop_wlen(t_attr *attr, int i, int j)
{
	if (i == 0)
	{
		if (j == 0)
			if (!(attr->flags & MINUS))
				attr->pwc_len = print_spaces(
					attr->width, attr->pwc_len, attr->flags);
		if (j == 2)
			if (!(attr->flags & MINUS))
				attr->pr_len = print_spaces(
					attr->width, attr->pr_len, attr->flags);
	}
	else
	{
		if (j == 1)
			if (attr->flags & MINUS)
				attr->pwc_len = print_spaces(
					attr->width, attr->pwc_len, attr->flags);
		if (j == 3)
			if (attr->flags & MINUS)
				attr->pr_len = print_spaces(
					attr->width, attr->pr_len, attr->flags);
	}
}

int			print_wide_character(va_list ap, t_attr *attr)
{
	attr->pwc = va_arg(ap, wint_t);
	attr->pwc_len = ft_wclen(attr->pwc);
	prop_wlen(attr, 0, 0);
	ft_putwchar(attr->pwc);
	prop_wlen(attr, 1, 1);
	return (attr->pwc_len);
}

int			print_string(va_list ap, t_attr *attr)
{
	if (attr->flags & L)
		return (print_wide_string(ap, attr));
	if (!(attr->ps_str = va_arg(ap, char *)))
	{
		attr->ps_str = (char*)malloc(sizeof(char) * ft_strlen("(null)"));
		ft_strcpy(attr->ps_str, "(null)");
		free(attr->ps_str);
	}
	if (attr->flags & PRECISION)
	{
		if (!(attr->ps_cpy = (char *)malloc(sizeof(char) * attr->precision)))
			return (0);
		attr->ps_cpy = ft_strncpy(attr->ps_cpy, attr->ps_str, attr->precision);
		attr->ps_str = attr->ps_cpy;
	}
	attr->ps_len = ft_strlen(attr->ps_str);
	if (!(attr->flags & MINUS))
		attr->ps_len = print_spaces(attr->width, attr->ps_len, attr->flags);
	ft_putstr(attr->ps_str);
	if (attr->flags & MINUS)
		attr->ps_len = print_spaces(attr->width, attr->ps_len, attr->flags);
	return (attr->ps_len);
}

int			print_character(va_list ap, t_attr *attr)
{
	if (attr->flags & L)
		return (print_wide_character(ap, attr));
	attr->pr_c = va_arg(ap, int);
	attr->pr_len = sizeof(char);
	prop_wlen(attr, 0, 2);
	ft_putchar(attr->pr_c);
	prop_wlen(attr, 1, 3);
	return (attr->pr_len);
}
