/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_nbr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 14:40:39 by ollevyts          #+#    #+#             */
/*   Updated: 2018/09/13 14:40:40 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

static char	*conv_dec(uintmax_t nbr, int *nbr_digits)
{
	char	*i;

	i = get_nbr_unsigned(nbr, nbr_digits, 10);
	return (i);
}

static int	signed_nbr(va_list ap, t_attr *attr)
{
	if (attr->flags & J || attr->flags & Z)
		attr->d_len = format_signed(va_arg(ap, intmax_t), attr);
	else if (attr->flags & LL)
		attr->d_len = format_signed(va_arg(ap, long long), attr);
	else if (attr->flags & L || attr->flags & UPP_D_BIT)
		attr->d_len = format_signed(va_arg(ap, long), attr);
	else if (attr->flags & HH)
		attr->d_len = format_signed((char)va_arg(ap, int), attr);
	else if (attr->flags & H)
		attr->d_len = format_signed((short)va_arg(ap, int), attr);
	else
		attr->d_len = format_signed(va_arg(ap, int), attr);
	return (attr->d_len);
}

static int	unsigned_nbr(va_list ap, t_attr *attr)
{
	if (attr->flags & J)
		attr->d_len = format_unsigned(
			va_arg(ap, uintmax_t), attr, &conv_dec);
	else if (attr->flags & Z)
		attr->d_len = format_unsigned(
			va_arg(ap, size_t), attr, &conv_dec);
	else if (attr->flags & LL)
		attr->d_len = format_unsigned(
			va_arg(ap, unsigned long long), attr, &conv_dec);
	else if (attr->flags & L || attr->flags & UPP_U_BIT)
		attr->d_len = format_unsigned(
			va_arg(ap, unsigned long), attr, &conv_dec);
	else if (attr->flags & HH)
		attr->d_len = format_unsigned(
			(unsigned char)va_arg(ap, unsigned int), attr, &conv_dec);
	else if (attr->flags & H)
		attr->d_len = format_unsigned(
			(unsigned short)va_arg(ap, unsigned int), attr,
			&conv_dec);
	else
		attr->d_len = format_unsigned(
			va_arg(ap, unsigned int), attr, &conv_dec);
	return (attr->d_len);
}

int			print_decimal(va_list ap, t_attr *attr)
{
	attr->d_len = 0;
	if (attr->flags & LOW_D_BIT ||
		attr->flags & LOW_I_BIT || attr->flags & UPP_D_BIT)
		attr->d_len = signed_nbr(ap, attr);
	if (attr->flags & LOW_U_BIT || attr->flags & UPP_U_BIT)
		attr->d_len = unsigned_nbr(ap, attr);
	return (attr->d_len);
}
